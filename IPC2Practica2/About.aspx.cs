﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using MySql.Data.MySqlClient;
using MySql.Data;



namespace IPC2Practica2
{
    
    public partial class About : Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            //  Conexion cx = new Conexion();
            // cx.Consulta("Select * from producto");
            msotrar();
        }
        public MySqlConnection Conectar()
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=1;database=practica2;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            databaseConnection.Open();
            return databaseConnection;
        }
        public void msotrar()
        {
            MySqlConnection databaseConnection= Conectar();
            string query = "select  * from   producto;";
           
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos
                

                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }

            /*Console.WriteLine("Iniciando la conexion Mysql");
            conexion.Open();
            //
            string sql = "show tables";
            MySqlCommand g = new MySqlCommand(sql, conexion);
            MySqlDataReader rdr = g.ExecuteReader();
            while (rdr.Read()) {
                Console.WriteLine(rdr[0]);
            }
            rdr.Close();
            conexion.Close();
            Console.WriteLine("Finalizacion de la conexion");*/
        }
    }
}