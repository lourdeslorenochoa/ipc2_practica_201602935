﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using MySql.Data;

namespace IPC2Practica2
{
    public class Conexion
    {
        public static MySqlConnection Conectar()
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=1;database=practica2;";
            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            databaseConnection.Open();
            return databaseConnection;
        }
        public static Boolean Loggin(String usuario, String pass)
        {

            MySqlConnection databaseConnection = Conectar();
            string query = "select  * from   usuario where usuario='"+usuario+"' and contraseña='"+pass+"';";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    return true;
                }
                return false;
            }
            catch
            {

            }
            return true;
        }

        public void msotrar()
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "select  * from   tipo;";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos


                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }

            
        }
        public static void insertarUsuario(string usuario, string pass, string nombre, string apellido)
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "insert into usuario (usuario, contraseña, nombre, apellido) values('" + usuario+"' ,'"+pass+"','"+nombre+"','"+apellido+"');";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos


                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }


        }
        public static void insertarProducto(string descripcion, int stock, int costoU, int codU, int codT, int codM, int codC)
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "insert into producto (descripcion, stock, costoUnidad, codU, codT, codM, codC) values('" + descripcion + "' ,'" + stock + "','" + costoU + "','" + codU + "', '" + codT + "', '" + codM + "', '" + codC + "');";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos


                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }
        }
        public static void insertarTipo(string nombre)
        {

            MySqlConnection databaseConnection = Conectar();
            string query = "insert into tipo (nombre) values('" + nombre +"')";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos


                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }
        }
        public static void insertarMaterial(string nombre)
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "insert into material (nombre) values('" + nombre + "')";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos


                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }
        }
        public static void insertarColor(string nombre)
        {
            MySqlConnection databaseConnection = Conectar();
            string query = "insert into color (nombre) values('" + nombre + "')";

            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;
            try
            {
                // Abre la base de datos


                // Ejecuta la consultas
                reader = commandDatabase.ExecuteReader();


                // Hasta el momento todo bien, es decir datos obtenidos

                // IMPORTANTE :#
                // Si tu consulta retorna un resultado, usa el siguiente proceso para obtener datos

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // En nuestra base de datos, el array contiene:  ID 0, FIRST_NAME 1,LAST_NAME 2
                        // Hacer algo con cada fila obtenida
                        string[] row = { reader.GetString(1) };
                        Console.WriteLine(row[0]);
                    }
                }
                else
                {
                    Console.WriteLine("No se encontraron datos.");
                }

                // Cerrar la conexión
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                // Mostrar cualquier excepción
                //MessageBox.Show(ex.Message);
            }
        }
    }
    
    }

    
