﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertarProducto.aspx.cs" Inherits="IPC2Practica2.InsertarProducto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
        <div class ="container">
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Descripción</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextDescripcion"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                </div>
            </div>

           <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Cantidad Disponible</label>
                <div class="col-sm-10">
                     <asp:TextBox ID="TextCantidad"  type="password" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                </div>
           </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Costo Por Unidad</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextCosto"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Código de Usuario</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextCodU"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                </div>
            </div>
             
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Código de Tipo</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextCodT"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                </div>
            </div>
            
                        <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Código de Material</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextCodM"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                </div>
            </div>
            
                        <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Código de Color</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextCodC"  type="text" class="form-control"  placeholder=" " runat="server"></asp:TextBox>
                </div>
            </div>
             <asp:Button ID="Button4" runat="server"  class="btn btn-primary" Text="Insertar"  />
       </div>
    </form>
</body>
</html>
