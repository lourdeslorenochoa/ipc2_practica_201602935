﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IPC2Practica2
{
    public partial class InsertarProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Conexion.insertarProducto(TextDescripcion.Text, int.Parse(TextCantidad.Text.ToString()),int.Parse( TextCosto.Text.ToString()), int.Parse(TextCodU.Text.ToString()), int.Parse(TextCodT.Text.ToString()), int.Parse(TextCodM.Text.ToString()), int.Parse(TextCodC.Text.ToString()));
            
            TextCodC.Text = "";
            TextCodT.Text = "";
            TextCodM.Text = "";
            TextDescripcion.Text = "";
            TextCantidad.Text = "";
            TextCosto.Text = "";
        }
    }
}