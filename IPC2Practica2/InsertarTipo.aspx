﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InsertarTipo.aspx.cs" Inherits="IPC2Practica2.InsertarTipo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class ="container">
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Tipo</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextNombre"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                </div>
                 <asp:Button ID="Button1" runat="server"  class="btn btn-primary" Text="Insertar" OnClick="Button1_Click"  />
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Material</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextMaterial"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                </div>
                 <asp:Button ID="Button2" runat="server"  class="btn btn-primary" Text="Insertar" OnClick="Button2_Click"  />
            </div>
            <div class="form-group row">
                <label for="staticEmail" class="col-sm-2 col-form-label">Color</label>
                <div class="col-sm-10">
                   <asp:TextBox ID="TextColor"  type="text" class="form-control"  placeholder="" runat="server"></asp:TextBox>
                </div>
                
            </div>
            <asp:Button ID="Button4" runat="server"  class="btn btn-primary" Text="Insertar" OnClick="Button3_Click"  />
        </div>
       
    </form>
</body>
</html>
