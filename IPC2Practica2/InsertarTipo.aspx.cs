﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IPC2Practica2
{
    public partial class InsertarTipo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Conexion.insertarTipo(TextNombre.Text);
            TextNombre.Text = " ";
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Conexion.insertarMaterial(TextMaterial.Text);
            TextMaterial.Text = " ";
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Conexion.insertarColor(TextColor.Text);
            TextColor.Text = " ";
        }
    }
}