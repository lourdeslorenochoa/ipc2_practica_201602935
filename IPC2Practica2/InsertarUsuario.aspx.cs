﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IPC2Practica2
{
    public partial class InsertarUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Conexion.insertarUsuario(TextUsuario.Text, TextPass.Text, TextNombre.Text, TextApellido.Text);
            TextUsuario.Text = " " ;
            TextPass.Text = "";
            TextNombre.Text = "";
            TextApellido.Text = "";
            Response.Redirect("usuario");
        }
    }
}