create database practica2;
use practica2;

create table tipo(
    codT int not null,
    nombre varchar(45) not null,
    primary key (codT)
);
create table material(
    codM int not null,
    nombre varchar(45) not null,
    primary key (codM)
);
create table color(
    codC int not null,
    nombre varchar(45) not null,
    primary key (codC)
);
create table usuario(
    codU int not null,
    nombre varchar(45) not null,
    primary key (codU)
);
create table producto(
    codP int not null,
    Descripcion varchar(45) not null,
    stock int not null,
    costoUnidad int not null,
    codU int not null,
    codT int not null,
    codM int not null,
    codC int not null,
    primary key (codP),
    index (codP),
    foreign key (codU) references usuario (codU),
    foreign key (codT) references tipo (codT),
    foreign key (codM) references material (codM),
    foreign key (codC) references color (codC)
);
